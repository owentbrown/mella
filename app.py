from chalice import Chalice

app = Chalice(app_name='sample')


@app.route('/')
def index():
    return {'hello': 'Try posting to /temperature instead'}


@app.route('/temperature', methods=['POST'])
def put_test():
    """Return the core body temperature.

    data has format
    {
        "weight": {
            "unit": "kilograms",
            "amount": 2.2
        },
        "ambient_temperature": {
            "unit"
        },
        "skin_temperature": {
            "unit": "celsius",
            "amount": 39.2
        }
    }

    Returns JSON in the format
    {
        "core_temperature" : {
            "unit": "celsius",
            "amount": 39.2
        }
    }
    """
    reading = app.current_request.json_body

    assert reading["weight"]["unit"] == "kilograms"
    assert reading["ambient_temperature"]["unit"] == "celsius"
    assert reading["skin_temperature"]["unit"] == "celsius"

    if reading["weight"]["amount"] > 10.0:
        core_temperature_celsius = reading["skin_temperature"]["amount"] + 1
    else:
        core_temperature_celsius = reading["skin_temperature"]["amount"]

    return {
        "core_temperature": {
            "unit": "celsius",
            "amount": core_temperature_celsius
        }
    }


