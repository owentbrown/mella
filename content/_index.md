## Mella AI

This project is scaffolding and proof-of-concept for a serverless deployment of Mella's API.


## API specification
This should really be a Swagger document.

### Request
POST to 
https://zfy18zbkxk.execute-api.us-west-2.amazonaws.com/api/temperature
 
```json
    {
        "weight": {
            "unit": "kilograms",
            "amount": 2.2
        },
        "ambient_temperature": {
            "unit"
        },
        "skin_temperature": {
            "unit": "celsius",
            "amount": 39.2
        }
    }
```

### Response

```json
    {
        "core_temperature" : {
            "unit": "celsius",
            "amount": 39.2
        }
    }
```

## What's next?
Check out our [issue tracker](https://gitlab.com/owentbrown/mella/-/issues)


